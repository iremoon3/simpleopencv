#pragma once

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <map>

using namespace cv;

class OpenCVHelper
{
	std::map<std::string, Mat> savedPics;

	bool CheckSavedImage(const std::string & from_name, const std::string & to_name) const;
public:
	void Load  (const std::string & name, const std::string & filename);
    void Store (const std::string & name, const std::string & filename);
    void Blur  (const std::string & from_name, const std::string & to_name, const std::string & size);
    void Resize(const std::string & from_name, const std::string & to_name, const std::string & new_width, const std::string & new_height);

	OpenCVHelper() {};
};