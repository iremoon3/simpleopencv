#include "OpenCVHelper.h"

#include "iostream"
#include "conio.h"

bool InputYN() {
	std::cout << "Input y/n: ";
	char c = _getch();
	if (c == 'y')
		return true;
	else if (c == 'n')
		return false;
	else
		InputYN();
}

void OpenCVHelper::Load(const std::string & name,const std::string & filename)
{
	if (savedPics.find(name) != savedPics.end()) {
		std::cout << "You really want reload image?" << '\n';
		if(!InputYN())
			return;
	}

	Mat src;
	src = imread(filename, CV_LOAD_IMAGE_COLOR);
	
	if (src.empty()) {
		std::cout << "Can not open or image is not present!" << '\n' << '\n';

		return;
	}

	savedPics.insert(std::pair<std::string, Mat>(name, src));

	std::cout << "Loaded " << filename << " as " << name << '\n' << '\n';
}

void OpenCVHelper::Store(const std::string & name, const std::string & filename)
{
	if (savedPics.find(name) == savedPics.end())
	{
		std::cout << "This image doesn't exists!" << '\n';
		return;
	}

	if (imwrite(filename, savedPics[name]))
		std::cout << "File " << name << " saved as " << filename << '\n' << '\n';
	else
		std::cout << "Error: Image didn't save!" << '\n' << '\n';
}

void OpenCVHelper::Blur(const std::string & from_name, const std::string & to_name, const std::string & size)
{
	if (!CheckSavedImage(from_name, to_name))
		return;

	Mat resultPic;
	blur(savedPics[from_name], resultPic, Size(std::stoi(size), std::stoi(size)));

	savedPics.insert(std::pair<std::string, Mat>(to_name, resultPic));
	std::cout << "The image " << from_name << " is blured to " << to_name << '\n' << '\n';
}

void OpenCVHelper::Resize(const std::string & from_name, const std::string & to_name,
						  const std::string & new_width, const std::string & new_height)
{
	if (!CheckSavedImage(from_name, to_name))
		return;

	Size size(std::stoi(new_width), std::stoi(new_height));
	Mat resultPic;
	resize(savedPics[from_name], resultPic, size);

	savedPics.insert(std::pair<std::string, Mat>(to_name, resultPic));
	std::cout << "Resized" << '\n' << '\n';
}

bool OpenCVHelper::CheckSavedImage(const std::string & from_name, const std::string & to_name) const
{
	if (savedPics.find(from_name) == savedPics.end())
	{
		std::cout << "This image doesn't exists!" << '\n';
		return false;
	}

	if (savedPics.find(to_name) != savedPics.end()) {
		std::cout << "You really want resave image?" << '\n';
		if (!InputYN())
			return false;
	}

	return true;
}