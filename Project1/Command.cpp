#include "Command.h"

bool is_number(const std::string& s)
{
	return !s.empty() && std::find_if(s.begin(),
		s.end(), [](unsigned char c) { return !std::isdigit(c); }) == s.end();
}

bool Command::CheckParam(const std::string & param,const Param_Type & type) const
{
	switch (type)
	{
		case Param_name:
			return true;// TODO?
		case Param_filename:
			return true;// TODO?
		case Param_to_name:
			return true;// TODO?
		case Param_from_name:
			return true;// TODO?
		case Param_size:
			return is_number(param);
		case Param_new_width:
			return is_number(param);
		case Param_new_height:
			return is_number(param);
		default:
			return false;
	}
}

bool Command::CheckAllParams(const std::vector<std::string> & allParams) const
{
	int i = 1;

	for (auto& param : params)
		if(!CheckParam(allParams[i++], param))
			return false;

	return true;
}

int Command::GetParamsNum () const
{
	if (params[0] != Param_no)
		return static_cast <int> (params.size());
	else
		return 0;
}