
#include "CommandWorker.h"
#include "OpenCVHelper.h"

int main()
{
	OpenCVHelper m_OpenCVHelper;
	CommandWorker m_CommandWorker(m_OpenCVHelper);

	return 0;
}