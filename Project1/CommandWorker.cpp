#include "CommandWorker.h"
#include "Command.h"
#include "iostream"

void CommandWorker::InitCommands()
{
    InitSingleCommand(Command_Load,   { "load", "ld" },        { Param_name, Param_filename });
    InitSingleCommand(Command_Store,  { "store", "s" },        { Param_name, Param_filename });
    InitSingleCommand(Command_Blur,   { "blur" },              { Param_from_name, Param_to_name, Param_size });
    InitSingleCommand(Command_Resize, { "resize" },            { Param_from_name, Param_to_name, Param_new_width, Param_new_height });
    InitSingleCommand(Command_Help,   { "help", "h" },         { Param_no });
    InitSingleCommand(Command_Quit,   { "exit", "quit", "q" }, { Param_no });
}

void CommandWorker::HandleCommand(const std::vector<std::string>& commandElements)
{
    if (!CheckCommand(commandElements[0])) {
        std::cout << "You input incorrect command!" << '\n';
        return;
    }

    Command command = GetCommand(commandElements[0]);

    int paramsNum = static_cast <int> (commandElements.size()) - 1;

    if (command.GetParamsNum() != paramsNum)
    {
        std::cout << "You input incorrect number of params!" << '\n';
        return;
    }
    
    if(command.GetParamsNum() != 0)
        if(!command.CheckAllParams(commandElements))
        {
            std::cout << "You input incorrect params!" << '\n';
            return;
        }

    Command_Type commandType = command.GetCommandType();

    switch (commandType)
    {
        case Command_Load:
            m_OpenCVHelper.Load(commandElements[1], commandElements[2]);
            break;
        case Command_Store:
            m_OpenCVHelper.Store(commandElements[1], commandElements[2]);
            break;
        case Command_Blur:
            m_OpenCVHelper.Blur(commandElements[1], commandElements[2], commandElements[3]);
            break;
        case Command_Resize:
            m_OpenCVHelper.Resize(commandElements[1], commandElements[2], commandElements[3], commandElements[4]);
            break;
        case Command_Help:
            PrintHelp();
            break;
        case Command_Quit:
            exit(EXIT_SUCCESS);
            break;
    }
}

CommandWorker::CommandWorker(const OpenCVHelper& m_OpenCVHelper) : m_OpenCVHelper(m_OpenCVHelper)
{
    InitCommands();
    InitAllCommandNames();

    while (1)
        InputCommand();
}

Command CommandWorker::GetCommand(const std::string & command)
{
    for (auto & c : allCommands)
        for (auto & v : c.GetCommandNameVars())
            if (command.compare(v) == 0)
                return c;
}

void CommandWorker::ParseInput(std::string& input)
{
    std::vector<std::string> inputElement;
    std::string delimiter = " ";

    size_t pos = 0;
    std::string token;
    while ((pos = input.find(delimiter)) != std::string::npos) {
        token = input.substr(0, pos);

        if(token.compare(" ") != 0)
            inputElement.push_back(token);

        input.erase(0, pos + delimiter.length());
    }

    if (token.compare(" ") != 0)
        inputElement.push_back(input);

    HandleCommand(inputElement);
}

bool CommandWorker::CheckCommand(const std::string & command) const
{
    for (auto & c : allCommandNames)
        if (c.compare(command) == 0)
            return true;

    return false;
}

void CommandWorker::InitAllCommandNames() 
{
    for (auto& c : allCommands)
        for (auto& v : c.GetCommandNameVars())
            allCommandNames.insert(v);
}

void CommandWorker::InputCommand()
{
    std::cout << "Input your command:" << '\n';

    std::string input;
    std::getline(std::cin, input);

    if(!input.empty())
        ParseInput(input);
    else
        std::cout << "Your input is empty!" << '\n';
}

void CommandWorker::PrintHelp() const
{
    std::cout << "load, ld - Loading the image in JPG format" << '\n' <<
        '\t' << "<name> - is the name of the image, by name it is available in other commands" << '\n' <<
        '\t' << "<filename> - file name to upload" << '\n';

    std::cout << "store, s - Save image in JPG format" << '\n' <<
        '\t' << "<name> - image name" << '\n' <<
        '\t' << "<filename> - file name to upload" << '\n';

    std::cout << "blur - Smoothing the image" << '\n' <<
        '\t' << "<from_name> - image name" << '\n' <<
        '\t' << "<to_name> - image name" << '\n' <<
        '\t' << "<size> - size of the area to be anti-aliased" << '\n';

    std::cout << "resize - Image resizing" << '\n' <<
        '\t' << "<from_name> - image name" << '\n' <<
        '\t' << "<to_name> - image name" << '\n' <<
        '\t' << "<new_width>" << '\n' <<
        '\t' << "<new_height>" << '\n';

    std::cout << "help, h - Displays help about supported commands" << '\n';

    std::cout << "exit, quit, q - exit from programm" << '\n' << '\n';
}