#pragma once

#include <vector>
#include <set>
#include <string>

enum Command_Type
{
	Command_Load,
	Command_Store,
	Command_Blur,
	Command_Resize,
	Command_Help,
	Command_Quit,
};

enum Param_Type
{
	Param_name,
	Param_filename,
	Param_from_name,
	Param_to_name,
	Param_size,
	Param_new_width,
	Param_new_height,

	Param_no,
};

class Command
{
	std::set<std::string> commandNameVars;
	std::vector<Param_Type> params;
	
	Command_Type commandType;

	bool CheckParam(const std::string & param, const Param_Type & type) const;
public:
	int  GetParamsNum() const; 
	bool CheckAllParams(const std::vector<std::string> & params) const;

	std::set<std::string> GetCommandNameVars() const { return commandNameVars; }
	Command_Type		  GetCommandType()	   const { return commandType;     }

	void InitCommandNameVars(const std::initializer_list<std::string> & names)
		{ for (auto name : names) commandNameVars.insert(name); }

	Command(const Command_Type & commandType, const std::initializer_list<const char*> & names,
		const std::initializer_list<Param_Type> & needParams) :
			commandType(commandType), commandNameVars(names.begin(), names.end()), params(needParams) {};
};

