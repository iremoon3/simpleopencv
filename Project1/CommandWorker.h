#pragma once

#include "OpenCVHelper.h"
#include "Command.h"

#include <set>
#include <vector>
#include <string>

class CommandWorker
{
	std::vector<Command> allCommands;
	std::set<std::string> allCommandNames;

	OpenCVHelper m_OpenCVHelper;

public:

	bool CheckCommand(const std::string & command) const;

	void InitSingleCommand(const Command_Type & commandType, const std::initializer_list<const char*> & names,
		const std::initializer_list<Param_Type> & needParams)
		{ allCommands.push_back(Command(commandType, names, needParams)); }

	void InitCommands();
	void InitAllCommandNames();
	void ParseInput(std::string & input);
	void HandleCommand(const std::vector<std::string> & commandElements);
	void PrintHelp() const;
	void InputCommand();
	Command GetCommand(const std::string& command);

	CommandWorker(const OpenCVHelper & m_OpecCVHelper);
};

